# hissyblog-freebsd-verify-zig

Simple Zig (pre-0.11.x) toolchain verification by way of hello world.

This forms part of a series of blog posts on [Hissyblog](https://hissyfit.gitlab.io/hissyblog/) regarding the use of FreeBSD for software development.

## Prerequisites

An installation of [Zig](https://ziglang.org).

## Building and Running

```shell
zig build run
```

## Copyright and License

Copyright © 2023 Kevin Poalses

Distributed under the [Simplified BSD License](./LICENSE.md).
