const std = @import("std");

pub fn main() !void {
    // Greet the world
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();
    try stdout.print("Hello, world!\n", .{});
    try bw.flush();
}
